"""
git-lab
"""

VERSION = (0, 7, 0)

__version__     = ".".join(map(str, VERSION[0:3]))
__author__      = "b-sh"
__contact__     = "b-sh@gmx.net"
__homepage__    = "https://gitlab.com/bor-sh/git-gitlab"
__download__    = "https://gitlab.com/bor-sh/git-gitlab/repository/archive.tar.gz?ref=v0.6"
__description__ = "Git extension commandline for GitLab API v3"
__docformat__   = "markdown"
__copyright__   = ""
__license__     = "MIT"
