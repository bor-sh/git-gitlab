## v0.6

* rename git-gitlab to git-lab
* Add new feature: "Issues". Allow to list and filter issues of project.

## v0.5

* Allow state as input closes #5
* Add usage and fixup update merge request unnecessary default branch closes #5
* Fixing non-latin-1 character printing in comments and usernames

## v0.4.1

* add usage info
* fixup unnecessary default target branch during update merge request

## v0.4

* update to user interface change in libsaas_gitlab v0.2 

## v0.3

* libsaas_gitlab usage
* create merge requests
* show infos merge requests
* post and show comments - merge request
* update merge request

